//Import thư viện mongoose
const mongoose = require("mongoose");

//Class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//Khởi tạo instance reviewSchema từ Class Schema
const reviewSchema = new Schema({
    stars: {
        type: Number,
        default: 0
    },
    note: {
        type: String,
        required: false
    }
}, {
    // Ghi dấu bản ghi được tạo hay cập nhật vào thời gian nào
    timestamps: true
});

// Biên dịch Review Model từ reviewSchema
module.exports = mongoose.model("Review", reviewSchema);